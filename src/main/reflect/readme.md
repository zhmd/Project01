##反射
###Java的反射是指程序在运行期可以拿到一个对象的所有信息

####而class是由JVM在执行过程中动态加载的。JVM在第一次读取到一种class类型时，将其加载进内存。
####  每加载一种class，JVM就为其创建一个Class类型的实例，并关联起来。注意：这里的Class类型是一个名叫Class的class

###Class访问对象信息API

###Field

####Field getField(name)：根据字段名获取某个public的field（包括父类）
####Field getDeclaredField(name)：根据字段名获取当前类的某个field（不包括父类）
####Field[] getFields()：获取所有public的field（包括父类）
####Field[] getDeclaredFields()：获取当前类的所有field（不包括父类）

####Field.setAccessible(true); 设置属性可访问 privite 默认不能访问

####get(obj) 返回字段的值obj为原对象实列
####getName()：返回字段名称，例如，"name"；
####getType()：返回字段类型，也是一个Class实例，例如，String.class；
####getModifiers()：返回字段的修饰符，它是一个int，不同的bit表示不同的含义。