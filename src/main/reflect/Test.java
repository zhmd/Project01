package main.reflect;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class Test {

    public static void main(String[] args) throws Exception {

        //正常获取一个对象
        Student student = new Student();

        //反射的方式获取对象
        //1. 通过已有对象获取
        Class clazz_1 = student.getClass();

        //2. 通过全类名获取
        try {
            Class clazz_2 = Class.forName("main.reflect.Student");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        //3.通过类名获取
        Class clazz_3 = Student.class;

        //Class 转换成对像
        try {
            Student student_c = (Student) clazz_3.newInstance();
            System.out.println(student_c.getName());
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        //TODO 关于属性
        Student stud = (Student) clazz_3.newInstance();
        //1.获取属性
        Field[] fields = clazz_3.getDeclaredFields();
        for (Field f : fields) {
            //设置属性可访问
            f.setAccessible(true);
            //获取属性的名称
            String fName = f.getName();
            //获取属性的值
            Object objValue = f.get(stud);
            //获取属性的修饰符 Modifier辅助类
            int modifiers = f.getModifiers();
            //获取属性的返回值
            Class type = f.getType();

            System.out.println("属性名称--->" + fName);
            System.out.println("属性值--->" + objValue);
            System.out.println("属性的修饰符--->" + modifiers);
            System.out.println("属性的返回值--->" + type);

            if (fName.equals("age")) {
                //修改属性值
                f.set(stud, 5);
                Object objModifyValue = f.get(stud);
                System.out.println("修改后的属性值--->" + objModifyValue);
            }
        }


        //TODo 关于方法

        //获取单个方法
        Method method = clazz_3.getDeclaredMethod("getName");
        //获取全部方法
        Method[] methods = clazz_3.getDeclaredMethods();

        for (Method m : methods) {
            m.setAccessible(true);
            //方法名称
            String mName= m.getName();
            System.out.println("方法名称---->"+mName);

            //其他方法 参考属性

            if(m.getName().equals("setName")){
                //方法执行
                m.invoke(stud,"张飞");
            }
        }

        System.out.println("名称--->"+stud.getName());
    }
}
