package main.thread.single;

/**
 * 线程的生命周期
 */
public class ThreadLifeCycle {


    public static void main(String[] args) throws InterruptedException {

        MyThread t = new MyThread();
        t.start();
        //当我们调用某个线程的这个方法时，这个方法会挂起调用线程，直到被调用线程结束执行，调用线程才会继续执行。
        t.join();

        //中断一个线程
//        t.interrupt();

        System.out.println("我是main()");
    }

    /**
     * 1.创建：new 线程对象 （java层创建，系统层面未创建）
     * 2.就绪：调用对象的start()进入
     * 3.运行：被CPU调度
     * 4.阻塞：调用sleep方法。 在阻塞时间到了之后，回到就绪状态(除了sleep(),还有wait())
     * 5.死亡：线程的代码执行结束，就正常死亡。 出现异常中断线程的执行，就非正常死亡
     */


    public static class MyThread extends Thread {
        @Override
        public void run() {
            super.run();

            System.out.println("执行一些代码");


            try {
                sleep(5000);
            } catch (Exception e) {
                e.printStackTrace();
            }

            System.out.println("执行一段逻辑");

        }
    }
}
