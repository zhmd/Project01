package main.thread.single;

import javax.security.auth.callback.Callback;

public class ThreadCreate {


    public static void main(String[] args) {

        /**
         * 创建一个线程
         * 1. 继承Thread类
         * 2. 实现Runnable接口
         * 3. 匿名类的方式
         */

        //1. 继承Thread类
        MyThread myThread = new MyThread();
        myThread.start();

        //2. 实现Runnable接口
        Thread thread = new Thread(new MyRunable());
        thread.start();

        //3. 匿名类的方式
        new Thread(){
            @Override
            public void run() {
                System.out.println("线程名称----》"+Thread.currentThread().getName());
            }
        }.start();

    }


    public static class MyRunable implements Runnable{

        @Override
        public void run() {
            System.out.println("我是实现Runnable接口创建的线程");
            System.out.println("线程名称----》"+Thread.currentThread().getName());
        }
    }


    public static class MyThread extends Thread{
        @Override
        public void run() {
            super.run();
            System.out.println("我是 extends Thread 实现的线程");
        }
    }
}
