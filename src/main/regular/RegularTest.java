package main.regular;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegularTest {

    public static void main(String[] args) {

        //正则的意义：匹配字符
        String regularStr_1 = "abc";
        String abc = "abc";
        System.out.println(abc.matches(regularStr_1));

        //匹配任意字符用 . 可以连续多次使用
        String regularStr_2 = "a..c";
        String abc_2 = "a6c";
        System.out.println("匹配任意字符--->" + abc_2.matches(regularStr_2));

        //匹配数字 用 \d
        String regularStr_3 = "a\\d\\dc";
        String abc_3 = "a66c";
        System.out.println("匹配数字--->" + abc_3.matches(regularStr_3));

        //匹配空格 \s
        String regularStr_4 = "a\\sc";
        String abc_4 = "a c";
        System.out.println("匹配空格--->" + abc_4.matches(regularStr_4));


        //匹配非数字 \D
        String regularStr_5 = "a\\Dc";
        String abc_5 = "a5c";
        System.out.println("匹配非数字--->" + abc_5.matches(regularStr_5));


        //重复匹配，匹配多个一致的 *
        String regularStr_6 = "a\\d*c";
        String abc_6 = "ac";
        System.out.println("重复匹配--->" + abc_6.matches(regularStr_6));

        //重复匹配，至少匹配一个 +
        String regularStr_7 = "a\\d+c";
        String abc_7 = "ac";
        System.out.println("重复匹配，至少匹配一个--->" + abc_7.matches(regularStr_7));

        //匹配一个，或者0个
        String regularStr_8 = "a\\d?c";
        String abc_8 = "a23c";
        System.out.println("匹配一个，或者0个--->" + abc_8.matches(regularStr_8));

        //精确匹配数字 {n}
        String regularStr_9 = "a\\d{3}c";
        String abc_9 = "a23c";
        System.out.println("精确匹配--->" + abc_9.matches(regularStr_9));

        //精确匹配n-m之间 {n,m}  {x,} x以上
        String regularStr_10 = "a\\d{3,4}c";
        String abc_10 = "a23145c";
        System.out.println("精确匹配--->" + abc_10.matches(regularStr_10));


        //匹配开头^ 匹配结尾$
        String regularStr_11 = "^a\\d{3,4}c$";
        String abc_11 = "a2314c";
        System.out.println("匹配开头^ 匹配结尾$--->" + abc_11.matches(regularStr_11));

        //匹配区域[]
        String regularStr_12 = "[0-9a-zA-B][s]";
        String abc_12 = "zs";
        System.out.println("匹配区域[]--->" + abc_12.matches(regularStr_12));

        //匹配非 [^1-9]
        String regularStr_13 = "[^0-9a-zA-B][s]";
        String abc_13 = "Zs";
        System.out.println("匹配区域[]--->" + abc_13.matches(regularStr_13));


        //分组匹配 用于提起字符串（）
        Pattern pattern = Pattern.compile("(\\d{3,4})\\-(\\d{7,8})");
        pattern.matcher("010-12345678").matches(); // true
        pattern.matcher("021-123456").matches(); // true
        pattern.matcher("022#1234567").matches(); // false
        // 获得Matcher对象:
        Matcher matcher = pattern.matcher("010-12345678");
        if (matcher.matches()) {
            String whole = matcher.group(0); // "010-12345678", 0表示匹配的整个字符串
            String area = matcher.group(1); // "010", 1表示匹配的第1个子串
            String tel = matcher.group(2); // "12345678", 2表示匹配的第2个子串
            System.out.println(whole);
            System.out.println(area);
            System.out.println(tel);
        }


        //非贪婪匹配 ?
        //默认正则是贪婪匹配（及合理情况下，尽可能多的匹配）
        Pattern pattern_1 = Pattern.compile("(\\d+?)(0*)");
        Matcher matcher_1 = pattern_1.matcher("1230000");
        if (matcher_1.matches()) {
            System.out.println("group1=" + matcher_1.group(1)); // "123"
            System.out.println("group2=" + matcher_1.group(2)); // "0000"
        }

    }
}
