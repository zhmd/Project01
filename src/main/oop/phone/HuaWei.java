package main.oop.phone;

/**
 * 这是其中一个手机品牌 继承自手机
 * implements 为实现接口的关键字，
 * 继承是单继承（一个类只能同时继承一个类）
 * 接口可以多实现（一个类实现多个接口，继承则是单继承）
 */
public class HuaWei extends Phone implements ISonyCamera, INet5GStandard {



    /**
     * 重写父类的开机方法
     *
     * @Override为代表重写的一个注解
     */
    @Override
    public void openPhone() {
        //super 可以看作父类的一个实例引用，此处的意思为  调用父类的openPhone（）
        super.openPhone();
        System.out.println("我是华为手机，开机过程省略");
    }


    /**
     * 注意：此方法是实现（重写） 父类的抽象方法，此处为必须实现（重写）
     */
    @Override
    public void call() {
        System.out.println("此处为华为手机的打电话方法");
    }


    @Override
    public void swithCamera() {
        //Todo 华为手机使用的索尼相机的独有方法
    }

    @Override
    public void openCamera() {
        //Todo 华为手机的打开相机的方法
    }

    @Override
    public void takePhoto() {
        //Todo 华为手机的拍照的方法
    }

    @Override
    public void cloaseCamera() {
        //Todo 华为手机的关闭相机的方法
    }


    @Override
    public void init() {
        //TODO 初始化5G
        System.out.println("初始化5G");
    }

    @Override
    public void connect() {

        System.out.println("连接5G");
    }

    @Override
    public void disConnect() {
        System.out.println("断开5G连接");
    }
}
