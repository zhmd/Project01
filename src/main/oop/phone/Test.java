package main.oop.phone;

public class Test {
    public static void main(String[] args) {

        //此处为多态的体现
        //声明了一个Phone的实例，但是真正赋值给它的是 它的一个子类（HuaWei）
        //此时phone其实代表的就是HuaWei（理论上可以使用华为的一切属性和方法）
        //但是phone的实列只是可以使用phone所拥有的方法和属性，可以把phone通过强制类型转换，转换为HuaWei
        Phone phone = new HuaWei();
        phone.openPhone();
        phone.call();
        phone.closePhone();


        //强制类型转换
        if (phone instanceof HuaWei) {
            //此处 instanceof 关键字 可以判断 第一个实例是否是属于 第二个类型
            //不是同一个类型的 实例，强行转换会抛出异常
            HuaWei huaWei = (HuaWei) phone;
            huaWei.swithCamera();
        }


        //多态不只是 子类与父类 ，接口也是支持（多态大部分用在接口实现上）
        INet5GStandard standard = new HuaWei();
        standard.init();
        standard.connect();
        standard.disConnect();


        //多态 体现了JAVA的动态性。很多时候一套标准类（或者接口），有多个子类（或者实现类），我们只需要知道当程序运行是 那个父类（或者接口）代表的是那个子类（或者实现类）就可以了。


    }
}
