package main.oop.phone;

/**
 * interface 为 接口修饰关键字 ,接口也可继承接口
 * 接口中的方法，实现类中必须重写（抽象类除外）
 */
public interface ISonyCamera extends ICamera{

    //此处说明接口中也可以使用属性，不过属性都是 public static 的
    int code = 0x11;
    String tag = "0x11";

    //索尼相机独有的方法
    void swithCamera();

}
