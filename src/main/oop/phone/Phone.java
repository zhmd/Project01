package main.oop.phone;

/**
 * 这是一个Phone的一个类
 * 手机包含 价钱 和名称 两个属性
 * <p>
 * <p>
 * abstract 表示抽象的
 * class 关键字 修饰一个类
 */
public abstract class Phone {

    /**
     * 手机价格
     */
    private double price;

    /**
     * 手机名称
     */
    private String name;


    /**
     * 此处为一个空的构造方法,
     * 构造方法也是方法，可以把构造方法看作一个 返回值是Phone的一个无名方法
     */
    public Phone() {

    }

    /**
     * 此处为有参构造方法，可以看作是是无参构造方法的重载方法（重载方法是指方法参数不同或者返回值不同，的同一个名字（构造方法可以看作都是无名方法）的方法）
     *
     * @param phonePrice
     * @param phoneName
     */
    public Phone(double phonePrice, String phoneName) {
        //this指 当前对象的引用，及this就是Phone的别名。
        this.price = phonePrice;
        this.name = phoneName;

    }


    /**
     * 包含开机方法
     */
    public void openPhone() {
        System.out.println("我要开机");
    }

    /**
     * 包含关机方法
     */
    public void closePhone() {
        System.out.println("我要关机");
    }

    /**
     * 打电话方法
     * 这是一个抽象方法，代表 继承该类的子类，必须实现的方法（子类也是抽象类，那么该方法是可不实现的）
     */
    public abstract void call();
}
