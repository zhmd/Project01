package main.oop.phone;

public interface ICamera {
    void openCamera();
    void takePhoto();
    void cloaseCamera();
}
