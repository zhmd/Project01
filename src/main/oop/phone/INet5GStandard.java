package main.oop.phone;

/**
 * 接口定义5G标准
 */
public interface INet5GStandard {

    void init();

    void connect();

    void disConnect();
}
