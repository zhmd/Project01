package main.exception;

public class Test {

    public static void main(String[] args) {

//        int i = 5/0;
        //上面代码运行就会出现 java.lang.ArithmeticException 这是0被做了除法，得出的算数异常。
        //程序出现异常，程序代码就会停止，不会向下继续执行。

        //使用 try{}catch(Exception e){}的代码块将可能出现异常的程序代码放入try{}就可以捕获出现的异常，代码进入catch（）{}中继续执行，不会打断代码的执行。
        try {
            //可能会出现异常的代码
            int i = 5/0;
        }catch (Exception exception){
            //打出异常的堆栈信息
            exception.printStackTrace();
            System.out.println("catch住了异常");
        }finally {
            System.out.println("finally{}中的代码肯定会被执行");
        }

        System.out.println("我是main()执行完毕的标志");
    }

}
