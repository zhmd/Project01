#java内置的数据结构 Collection 与 Map

##Collection接口:
### 常用字接口：
### (1)List:实现类 ArrayList、Vector、LinkedList
####List 特点：有序
##### i. ArrayList:
#####   1. 底层数据结构是数组，查询快、增删慢
#####   2. 线程不安全，效率高
##### ii. Vector：
#####   1. 底层数据结构是数组，查询快，增删慢
#####   2. 线程安全，效率底
##### iii. LinkedList：
#####  1. 底层数据结构是链表，查询慢，增删快
#####  2. 线程不安全，效率高

### (2)Set:实现类：HashSet、TreeSet
####Set 特点：无序
#####i. HashSet (子类LinkedHashSet类有序的)
#####   1. 底层数据是哈希表
#####   2. 通过两个方法hashCode()和equals()保证元素的唯一性，方法自动生成
#####   3. 子类LinkedHashSet底层数据结构是链表和哈希表，由链表保证元素有序，由哈希表保证元素唯一。
#####ii. TreeSet (保证元素唯一性)
#####   1. 底层数据是红黑二叉树
#####   2. 排序方式：自然排序、比较器排序
#####   3. 通过比较返回值是否为0来保证元素的唯一性。



###Collection：集合（一般不直接用）
###List ：可重复，有序
###Set：不重复，无序
###Map：键值对

###ArrayList：效率高
###Vector ：效率低
###Stack ：后进先出

###HashSet：不重复
###LinkedHashSet：不重复，链表
###TreeSet：不重复，有序

###HashMap ：无序
###TreeMap ：有序
###HashTable：有序

