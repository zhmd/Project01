package main.datastructure;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Stack;

public class Test {

    public static void main(String[] args) {

        //ArrayList
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("hello");
        arrayList.addAll(new ArrayList<>());
        arrayList.size();
        arrayList.get(0);
        arrayList.clear();

        //Stack
        Stack<String> stack = new Stack();
        //入栈
        stack.push("hello");
        //出栈
        stack.pop();
        stack.size();
        stack.empty();


        //HashMap
        HashMap<String,String> hashMap = new HashMap();
        hashMap.put("key","value");
        hashMap.remove("key");
        hashMap.size();



    }
}
