###注解
####元注解：是用于修饰注解的注解，通常用在注解的定义上
#####@Target：注解的作用目标
#####@Target 注解标记另一个注解，以限制可以应用注解的Java元素类型。目标注解指定以下元素类型之一作为其值
######ElementType.TYPE 可以应用于类的任何元素。
######ElementType.FIELD 可以应用于字段或属性。
######ElementType.METHOD 可以应用于方法级注解。
######ElementType.PARAMETER 可以应用于方法的参数。
######ElementType.CONSTRUCTOR 可以应用于构造函数。
######ElementType.LOCAL_VARIABLE 可以应用于局部变量。
######ElementType.ANNOTATION_TYPE 可以应用于注解类型。
######ElementType.PACKAGE 可以应用于包声明。
######ElementType.TYPE_PARAMETER
######ElementType.TYPE_USE

###
#####@Retention：注解的生命周期
######RetentionPolicy.SOURCE - 标记的注解仅保留在源级别中，并由编译器忽略。
######RetentionPolicy.CLASS - 标记的注解在编译时由编译器保留，但Java虚拟机（JVM）会忽略。
######RetentionPolicy.RUNTIME - 标记的注解由JVM保留，因此运行时环境可以使用它。

#####@Documented：注解是否应当被包含在 JavaDoc 文档中
#####@Inherited：是否允许子类继承该注解